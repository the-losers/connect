package com.conecttechno.justchat.Activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.conecttechno.justchat.BaseParser.SignupParser;
import com.conecttechno.justchat.Helper.Constant;
import com.conecttechno.justchat.Http.AsyncTaskListener;
import com.conecttechno.justchat.Http.BaseParser;
import com.conecttechno.justchat.Http.HttpAsyncRequest;
import com.conecttechno.justchat.Http.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import conecttechno.justchat.R;

public class SignupActivity extends AppCompatActivity implements  View.OnClickListener{
Button datepicker,Signup;
    private int mYear,mMonth,mDay;
    EditText email,firstpassword,secondpassword;
RadioButton male,female;
    boolean validate = true;
    SharedPreferences.Editor edit;
    SharedPreferences pref ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        init();
        male.setChecked(true);
        datepicker.setOnClickListener(this);


Signup.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        validate = true;
        if(!isEmailValid(email.getText().toString().trim()))
        {
            email.setError("Wrong email format");
            validate = false;
        }

        if(!firstpassword.getText().toString().trim().equals(secondpassword.getText().toString().trim()))
        {
            firstpassword.setError("Password not match");
            validate = false;
        }
        if(firstpassword.getText().length() == 0)
        {

            firstpassword.setError("Password Cannot be empty");
            validate = false;

        }
        if( datepicker.getText().equals("Date Picker"))
        {
            validate = false;
            Toast.makeText(SignupActivity.this, "pick a Date of birth", Toast.LENGTH_SHORT).show();
        }
        if(validate)
        {
            if(male.isChecked()) {
                triggertoserver(email.getText().toString().trim(), firstpassword.getText().toString().trim(),datepicker.getText().toString().trim(), "male");
            }
            else{

                triggertoserver(email.getText().toString().trim(),firstpassword.getText().toString().trim(),datepicker.getText().toString().trim(),"female");
            }

        }
    }
});





}

void triggertoserver(String email,String password,String dob,String gender)
{

    //Toast.makeText(this, email+password+gender+dob, Toast.LENGTH_SHORT).show();
    HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.SIGNUPURL, HttpAsyncRequest.RequestType.POST, new SignupParser(), listener);
   request.addParam("email",email);
    request.addParam("password",password);
    request.addParam("gender",gender);
    request.addParam("key","signup");
    request.addParam("dob",dob);

    request.execute();


}
    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            //  viewProgressBar.setVisibility(View.INVISIBLE);
            if (result.isSuccess()) {
                if(result.getMessage().equals("1"))
                {
                    Toast.makeText(SignupActivity.this, "Account Created", Toast.LENGTH_SHORT).show();
                    edit.putString("email",email.getText().toString().trim());
                    edit.commit();
                    startActivity(new Intent(SignupActivity.this,ValidationActivitiy.class));
                    finish();



                }
                else  if(result.getMessage().equals("2")){

                    email.setError("Email Already exsist");
                }
                else{
                    Toast.makeText(SignupActivity.this, "error in communicating with server", Toast.LENGTH_SHORT).show();
                }
               // Toast.makeText(SignupActivity.this, "done", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(SignupActivity.this, "error", Toast.LENGTH_SHORT).show();
            }

        }

    };

    void init()
    {
        datepicker = (Button)findViewById(R.id.signup_datepicker);
        email = (EditText)findViewById(R.id.signup_emailaddress);
        firstpassword = (EditText)findViewById(R.id.signup_firstpassword);
        secondpassword= (EditText)findViewById(R.id.signup_secondpassword);
        male = (RadioButton)findViewById(R.id.signup_radiomale);
        female = (RadioButton)findViewById(R.id.signup_radiofemale);
        Signup = (Button)findViewById(R.id.signup_submitbutton);
        pref = getSharedPreferences("email", Context.MODE_PRIVATE);
        edit= pref.edit();


    }

    @Override
    public void onClick(View v) {

        if (v == datepicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                           // txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            Toast.makeText(SignupActivity.this,dayOfMonth+"/"+monthOfYear+"/"+year, Toast.LENGTH_SHORT).show();
                    datepicker.setText(dayOfMonth+"/"+monthOfYear+"/"+year);
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }

    }

    public static boolean isEmailValid(String email) {
        return !(email == null || TextUtils.isEmpty(email)) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


}