package com.conecttechno.justchat.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.conecttechno.justchat.BaseParser.SignupParser;
import com.conecttechno.justchat.BaseParser.ValidationParser;
import com.conecttechno.justchat.Helper.Constant;
import com.conecttechno.justchat.Http.AsyncTaskListener;
import com.conecttechno.justchat.Http.HttpAsyncRequest;
import com.conecttechno.justchat.Http.TaskResult;

import conecttechno.justchat.R;

public class ValidationActivitiy extends AppCompatActivity {
    Button submit, resendcode;
    EditText texttoverify;
    SharedPreferences pref;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation_activitiy);
        init();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (texttoverify.getText().length() != 0) {
                    // Posttoserver();
                    if (!pref.getString("email", null).equals(null))
                        Posttoserver();

                } else {
                    texttoverify.setError("Cannot be blank");

                }
            }
        });

        resendcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpAsyncRequest request = new HttpAsyncRequest(ValidationActivitiy.this, Constant.RESENDURL, HttpAsyncRequest.RequestType.POST, new ValidationParser(), listener1);
                request.addParam("key", "resend");
                request.addParam("email", pref.getString("email", null));


                request.execute();
            }
        });


    }

    void Posttoserver() {

        HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.VALIDATIONURL, HttpAsyncRequest.RequestType.POST, new ValidationParser(), listener);
        request.addParam("key", "validation");
        request.addParam("email", pref.getString("email", null));
        request.addParam("code", texttoverify.getText().toString().trim());

        request.execute();


    }

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            //  viewProgressBar.setVisibility(View.INVISIBLE);
            if (result.isSuccess()) {
                if (result.getMessage().equals("1")) {
                    Toast.makeText(ValidationActivitiy.this, "Account validation Completed", Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(ValidationActivitiy.this, EditProfileActivity.class));
                    finish();


                } else if (result.getMessage().equals("0")) {

                    texttoverify.setError("Wrong Code Enter");
                } else {
                    Toast.makeText(ValidationActivitiy.this, "error in communicating with server", Toast.LENGTH_SHORT).show();
                }
                // Toast.makeText(SignupActivity.this, "done", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ValidationActivitiy.this, "error", Toast.LENGTH_SHORT).show();
            }

        }

    };
    AsyncTaskListener listener1 = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            //  viewProgressBar.setVisibility(View.INVISIBLE);
            if (result.isSuccess()) {
                if (result.getMessage().equals("1")) {
                    Toast.makeText(ValidationActivitiy.this, "Email is send to you check your email", Toast.LENGTH_SHORT).show();

                    //   startActivity(new Intent(SignupActivity.this,ValidationActivitiy.class));


                } else if (result.getMessage().equals("0")) {

                    texttoverify.setError("Error occur with server connection");
                } else {
                    Toast.makeText(ValidationActivitiy.this, "error in communicating with server", Toast.LENGTH_SHORT).show();
                }
                // Toast.makeText(SignupActivity.this, "done", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ValidationActivitiy.this, "error", Toast.LENGTH_SHORT).show();
            }

        }

    };

    void init() {
        submit = (Button) findViewById(R.id.validation_submit);
        texttoverify = (EditText) findViewById(R.id.valiadtion_keytoverify);
        resendcode = (Button) findViewById(R.id.validation_resendemail);
        pref = getSharedPreferences("email", Context.MODE_PRIVATE);
        edit = pref.edit();


    }
}
