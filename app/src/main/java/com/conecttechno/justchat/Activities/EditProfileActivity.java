package com.conecttechno.justchat.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.conecttechno.justchat.BaseParser.ValidationParser;
import com.conecttechno.justchat.Helper.Constant;
import com.conecttechno.justchat.Http.AsyncTaskListener;
import com.conecttechno.justchat.Http.HttpAsyncRequest;
import com.conecttechno.justchat.Http.TaskResult;

import java.io.FileNotFoundException;
import java.io.InputStream;

import conecttechno.justchat.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity {
Spinner spinner;
    Button chooseimage,submitbutton;
    CircleImageView profilepic;
    EditText status,name,mobileno,institute,job;
    RadioGroup interest;
    String[] ITEMS = {"Single","Married","Complicated","Divorced","Enggaged"};
    String TAG = "EDITPROFILELOG";
    final int REQUEST_CODE_GALLERY = 999;
    SharedPreferences pref ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        init();
       submitbutton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if(verify())
               {
           Log.d(TAG,status.getText().toString());
                   Log.d(TAG,name.getText().toString());
                   Log.d(TAG,mobileno.getText().toString());
                   Log.d(TAG,interest.getCheckedRadioButtonId()+"");
                   Log.d(TAG,ITEMS[spinner.getSelectedItemPosition()]);
                   RadioButton btn = (RadioButton) findViewById(interest.getCheckedRadioButtonId());
                   Log.d(TAG,btn.getText().toString());




               }
           }
       });

chooseimage.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        ActivityCompat.requestPermissions(EditProfileActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_GALLERY);
    }
});

    }
    void init()
    {
      spinner = (Spinner) findViewById(R.id.spinner);
        chooseimage = (Button)findViewById(R.id.chooseimage);
        profilepic = (CircleImageView) findViewById(R.id.profile_image);
        status = (EditText)findViewById(R.id.editprofile_status);
        name = (EditText)findViewById(R.id.editprofile_name);
        mobileno = (EditText)findViewById(R.id.editprofile_phonenumber);
        interest = (RadioGroup) findViewById(R.id.group);
        institute  = (EditText)findViewById(R.id.editprofile_university);
        job = (EditText)findViewById(R.id.editprofile_job);
        submitbutton = (Button)findViewById(R.id.editprofile_submitbutton);
        pref = getSharedPreferences("email", Context.MODE_PRIVATE);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
 }
 boolean verify()
 {
     boolean check = true;

     if(status.getText().length() == 0)
     {
         check= false;
         status.setError("Status cannot be empty ");
     }
     if(name.getText().length() == 0)
     {
         check= false;
         name.setError("Name cannot be empty ");

     }
     if(mobileno.getText().length() == 0)
     {
         check= false;
         mobileno.setError("Mobile Number cannot be empty ");
     }
 return  check;



 }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    private String logoPath;

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            logoPath = getRealPathFromURI(uri);

            Toast.makeText(getApplicationContext(), logoPath, Toast.LENGTH_SHORT).show();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                Bitmap bt = Bitmap.createScaledBitmap(bitmap, 150, 150, false);
                profilepic.setImageBitmap(bt);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
void postDatatoserver(String interest,String relationship)
{



    HttpAsyncRequest request = new HttpAsyncRequest(this, Constant.EDITPROFILEURL, HttpAsyncRequest.RequestType.POST, new ValidationParser(), listener);
    request.addParam("key", " profile");
    request.addParam("email", pref.getString("email", null));

    request.addParam("name", name.getText().toString().trim());
    request.addParam("phone", mobileno.getText().toString().trim());
    request.addParam("interest", interest);
    request.addParam("relationship",relationship);
    request.addParam("job", job.getText().toString().trim());

    request.addParam("institute", job.getText().toString().trim());
    request.addFile("img",logoPath);
    request.execute();
}

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            //  viewProgressBar.setVisibility(View.INVISIBLE);
            if (result.isSuccess()) {
                if (result.getMessage().equals("1")) {
                    Toast.makeText(EditProfileActivity.this, "Account validation Completed", Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(EditProfileActivity.this, EditProfileActivity.class));
                    finish();


                } else if (result.getMessage().equals("0")) {

                   // texttoverify.setError("Wrong Code Enter");
                } else {
                   // Toast.makeText(ValidationActivitiy.this, "error in communicating with server", Toast.LENGTH_SHORT).show();
                }
                // Toast.makeText(SignupActivity.this, "done", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(ValidationActivitiy.this, "error", Toast.LENGTH_SHORT).show();
            }

        }

    };
}
